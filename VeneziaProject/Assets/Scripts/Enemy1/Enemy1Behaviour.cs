using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Enemy1Behaviour : MonoBehaviour
{
    [Header("Enemy Behaviour:")]
    [Tooltip("ampiezza verticale dell'OSCILLAZIONE del nemico")]
    public float high;
    [Tooltip("velocit� (frequenza) del movimento SINUSOIDALE del nemico")]
    public float frequency = 0f;
    [Tooltip("velocit� di movimento ORIZZONTALE del nemico")]
    public float enemySpeed = 1f;
    [Tooltip("velocit� di fuoco del nemico")]
    public float FireRate = 1f;

    public float PIdiff;

    public GameObject Enemy1Projectile;
    public Transform FirePointEnemy1;
    public GameObject DeathExplosion;

    private bool canFire = true;
    public bool canBeKilled = false;
    private Camera cam;
    private Enemy1Manager enemyManager;
    private SpriteRenderer spriteRenderer;

    public float BeeLifes = 2f;
    private void Awake()
    {
        enemyManager = FindObjectOfType<Enemy1Manager>();
        cam = Camera.main;
        spriteRenderer= GetComponent<SpriteRenderer>();

    }
    private void Start()
    {
        //conferisce un valore di PI diverso per ogni nemico istanziato per creare l'effetto onda (vedere Enemy1Manager)
        if (enemyManager.enemySpawned == 1)
        {
            PIdiff = 1;
        }
        if (enemyManager.enemySpawned == 2)
        {
            PIdiff = 0.8f;
        }
        if (enemyManager.enemySpawned == 3)
        {
            PIdiff = 0.5f;
        }
        if (enemyManager.enemySpawned == 4)
        {
            PIdiff = 0.2f;
        }
        if(enemyManager.enemySpawned == 5)
        {
            PIdiff = 0;
        }
        //frequenza del movimento nemico, regolato da Enemy1Manager a seconda della Wave
        frequency = enemyManager.enemyFrequency;

        //ampiezza verticale del movimento nemico, regolato da Enemy1Manager a seconda della Wave
        high = enemyManager.enemyHigh;

        //FireRate del nemico, regolato da Enemy1Manager a seconda della Wave
        FireRate = enemyManager.enemyFireRate;

        BeeLifes = 2f;

        
    }
    private void Update()
    {
        //pattern di movimento del nemico derivante dalla funzione seno
        Vector2 newPosition = new Vector2();
        newPosition.y = high * Mathf.Sin(Time.time * frequency + Mathf.PI * PIdiff); 
        newPosition.x = transform.position.x -(enemySpeed) * Time.deltaTime;
        transform.position = newPosition;

        //controllando che il nemico sia dentro il range della telecamera per iniziare a sparare
        Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
        if (viewPos.x < 1.02f && viewPos.x > 0 && canFire)
        {
            //meccanica di fuoco nemico
            canBeKilled= true;
            canFire = false;
            Instantiate(Enemy1Projectile, FirePointEnemy1.position, FirePointEnemy1.rotation);         

            StartCoroutine(DelayedAction(FireRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canFire = true;
            }
        }
        

        

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disattivare i nemici uccisi (possono essere colpiti solo se all'interno della telecamera
        if (collision.tag == "Bullet" && canBeKilled)
        {
            BeeLifes--;
            StartCoroutine(FlashRed());
          
            if (BeeLifes <= 0)
            {
                Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                Debug.Log("enemy ded");
            }
        }
        //controllo collisioni per disattivare i nemici non uccisi
        if (collision.tag == "EnemyBulletDestroy")
        {
            Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
            Debug.Log("enemy ded");

        }
    }

    private IEnumerator FlashRed()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
    }
}

