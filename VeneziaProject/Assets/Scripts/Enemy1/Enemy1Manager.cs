using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Enemy1Manager : MonoBehaviour
{
    public GameObject enemy1;
    public Transform Wave1Transform;
    public Transform Wave2Transform;
    public Transform Wave3Transform;

    private Enemy1Behaviour enemyB;

    
   

    public float enemySpawned = 0f;

    private bool canSpawn = true;

    public float spawnRate = 0f;
    public float enemyFrequency = 1f;
    public float enemyHigh = 2f;
    public float enemyFireRate;

    private bool firstWaveEnded = true;
    private bool secondWaveEnded = true;
    private bool thirdWaveEnded = true;

    private void Awake()
    {
        enemyB = FindObjectOfType<Enemy1Behaviour>();
        
    }
    private void Start()
    {

        
        
        
    }
    private void Update()
    {
       
        
        
        FirstWave();

        SecondWave();

        ThirdWave();
    }

    private void FirstWave()
    {
        //spawn della prima wave con float che regolano il behaviour dei nemici instanziati singolarmente / spawnRate e numero di nemici regolabili
        if (canSpawn && firstWaveEnded == false)
        {
            canSpawn = false;
            enemySpawned++;
            enemyFrequency = 3.5f;
            enemyHigh = 2f;
            enemyFireRate = 2f;
            Instantiate(enemy1, Wave1Transform.position, Wave1Transform.rotation);
            if (enemySpawned == 5)
            {
                firstWaveEnded = true;
                enemySpawned= 0;
            }

            StartCoroutine(DelayedAction(spawnRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canSpawn = true;
            }
        }
    }

    private void SecondWave()
    {
        //spawn della seconda wave con float che regolano il behaviour dei nemici instanziati singolarmente / spawnRate e numero di nemici regolabili
        if (canSpawn && secondWaveEnded == false)
        {
            canSpawn = false;
            enemySpawned++;
            enemyFrequency = 3.5f;
            enemyHigh = 3.25f;
            enemyFireRate = 1f;
            Instantiate(enemy1, Wave2Transform.position, Wave2Transform.rotation);
            if (enemySpawned == 5)
            {
                secondWaveEnded = true;
                enemySpawned = 0;
            }

            StartCoroutine(DelayedAction(spawnRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canSpawn = true;
            }
        }
    }
    private void ThirdWave()
    {
        //spawn della seconda wave con float che regolano il behaviour dei nemici instanziati singolarmente / spawnRate e numero di nemici regolabili
        if (canSpawn && thirdWaveEnded == false)
        {
            canSpawn = false;
            enemySpawned++;
            enemyFrequency = 3f;
            enemyHigh = 3f;
            enemyFireRate = 0.75f;
            
            Instantiate(enemy1, Wave3Transform.position, Wave3Transform.rotation);
            if (enemySpawned == 5)
            {
                thirdWaveEnded = true;
                enemySpawned = 0;
            }

            StartCoroutine(DelayedAction(spawnRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canSpawn = true;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo triggerBox per startare una wave, checkare la distanza dal punto 0,0 mi pesava troppo lazyass
        if(collision.name == "FirstWaveEnemy1")
        {
            firstWaveEnded = false;
        }
        if (collision.name == "SecondWaveEnemy1")
        {
            secondWaveEnded = false;
        }
        if (collision.name == "ThirdWaveEnemy1")
        {
            thirdWaveEnded = false;
        }
    }

}


