using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1PrimaryFire : MonoBehaviour
{
    private Rigidbody2D rb;
    public float bulletSpeed;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        //velocit� (regolabile) e direzione del proiettile nemico
        rb.velocity = Vector2.left * bulletSpeed;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disabilitare i proiettili
        if (collision.tag == "Player" || collision.tag == "Ground" || collision.tag == "EnemyBulletDestroy")
        {
            gameObject.SetActive(false);

        }
    }
}
