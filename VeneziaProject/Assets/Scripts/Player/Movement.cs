using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private float horizontalMovement = 0f;
    private float verticalMovement = 0f;
    private float movementSmooth = 0f;

    private bool StartingAnim = false;
    

    private Vector3 currentVel = Vector3.zero;
    private Vector3 targetPos = new Vector2(6, 0);

    private Rigidbody2D rb;
    private CameraBehaviour cb;
    private Weapon weapon;
    private GameManager gm;
    private Animator animator;

    [Header("Speed")]
    [SerializeField] float movementSpeed = 0f;
    public Collider2D playerTriggerCollider;
    public GameObject DeathExplosion;
    public GameObject CameraBoxCollider;

    


    private void Awake()
    {
        rb= GetComponent<Rigidbody2D>();
        cb = FindObjectOfType<CameraBehaviour>();
        weapon = FindObjectOfType<Weapon>();
        gm = FindObjectOfType<GameManager>();
        animator= GetComponent<Animator>();

    }
    private void Start()
    {
        transform.position = new Vector3 (-10, 0 ,0);
        cb.CameraSpeed = 0f;
        StartingAnim= true;
        CameraBoxCollider.SetActive(false);
        weapon.enabled= false;
        
    }

    private void Update()
    {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        verticalMovement = Input.GetAxisRaw("Vertical");

        //velocit� costante del player insieme alla telecamera (vedere CameraBehaviour)
        gameObject.transform.Translate(new Vector2(cb.CameraSpeed, 0) * Time.deltaTime, Space.World);

        //"animazione" iniziale (blocca il movimento, l'abilit� di sparare e il movimento della cam
        if (StartingAnim)
        {
            
            float step = movementSpeed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, targetPos, step);
            if(transform.position == targetPos)
            {
                targetPos = Vector3.zero;
                movementSpeed = 3f;

                if(transform.position == targetPos)
                {
                    CameraBoxCollider.SetActive(true);
                    weapon.enabled= true;
                    StartingAnim = false;
                    movementSpeed = 5f;
                    cb.CameraSpeed = 0.75f;
                }
            }
        }

        

    }
    private void FixedUpdate()
    {
        if (!StartingAnim)
        {
        //Calcolo vettore movimento
        Vector3 targetVel = new Vector2(horizontalMovement * Time.fixedDeltaTime * 100f, verticalMovement * Time.fixedDeltaTime * 100f);

        //Ricordate di normalizzare i vostri vettori bimbi altrimenti vi mangio le budella
        targetVel.Normalize();

        //SmoothDamp finale * movementSpeed regolabile
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVel, ref currentVel, movementSmooth) * movementSpeed;
        }
        
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni contro muro, proiettile o nemico / morte / Stop della camera
        if(collision.tag == "Creeper")
        {
            playerTriggerCollider.enabled = false;
            gm.playerLifes -= 2;
            animator.SetTrigger("Damaged");
            if (gm.playerLifes <= 0)
            {
                gm.playerDed = true;
                Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                cb.CameraSpeed = 0f;
            }
            if (!gm.playerDed)
            {
                StartCoroutine(DelayedAction(2));
                IEnumerator DelayedAction(float time)
                {
                    yield return new WaitForSeconds(time);

                    playerTriggerCollider.enabled = true;
                }
            }
        }
        if(collision.tag == "Enemy" || collision.tag == "EnemyBullet")
        {
            
            playerTriggerCollider.enabled= false;
            gm.playerLifes -= 1;
            animator.SetTrigger("Damaged");
            

            if (gm.playerLifes <= 0)
            {
                gm.playerDed = true;
                Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                cb.CameraSpeed = 0f;
            }
            if (!gm.playerDed)
            {
                StartCoroutine(DelayedAction(2));
                IEnumerator DelayedAction(float time)
                {
                    yield return new WaitForSeconds(time);

                    playerTriggerCollider.enabled = true;
                }
            }          
        }
        if (collision.tag == "GroundDeath")
        {
            gm.playerDed = true;
            Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
            cb.CameraSpeed = 0f;
        }
    }
    private void WeaponEnable()
    {
        weapon.enabled = true;
    }
    public void WeaponDisable()
    {
        weapon.enabled = false;
        Invoke("WeaponEnable", 2f);
    }

}
