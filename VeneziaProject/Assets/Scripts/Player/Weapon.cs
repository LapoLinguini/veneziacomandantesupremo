using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform FirePoint;
    public GameObject bullet1;
    public float FireRate = 0.2f;
    private Animator animator;
    private bool canFire = true;

    private void Awake()
    {
        animator= GetComponent<Animator>();
    }
    private void Update()
    {
        PrimaryFire();
        
    }
    private void PrimaryFire()
    {
        //meccanica di fuoco del player con FireRate regolabile
        if (Input.GetKey(KeyCode.Space) && canFire)
        {
            canFire= false;
            Instantiate(bullet1, FirePoint.position, FirePoint.rotation);
            animator.SetTrigger("IsShooting");

            StartCoroutine(DelayedAction(FireRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canFire= true;
            }
        }
    }
   
}
