using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimaryFire : MonoBehaviour
{
    private Rigidbody2D rb;

    public float bulletSpeed;

    private void Start()
    {
        rb= GetComponent<Rigidbody2D>();

        //velocit� (regolabile) e direzione del proiettile del player
        rb.velocity = Vector2.right * bulletSpeed;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disabilitare i proiettili
        if(collision.tag == "BulletDestroy" || collision.tag == "Ground" || collision.tag == "Enemy")
        {
            gameObject.SetActive(false);
            
        }
    }

}
