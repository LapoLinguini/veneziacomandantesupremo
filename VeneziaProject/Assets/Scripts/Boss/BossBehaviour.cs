using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    [Header("Enemy Behaviour:")]
    

  
    [Tooltip("velocit� di fuoco del nemico")]
    public float FireRate = 1f;

   

    public GameObject Enemy1Projectile;
    public GameObject Enemy2Projectile;
    public GameObject Enemy3Projectile;
    public Transform FirePointEnemy1;
    public GameObject DeathExplosion;
    private GameManager gm;
    private Animation anim;

    private CameraBehaviour cb;
    private bool canFire = true;
    public bool canBeKilled = false;
    private Camera cam;
    private bool SecondPhaseChange = false;
    private Collider2D bossCollider;
   
    private SpriteRenderer spriteRenderer;

    public float BossLifes = 85f;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        cam = Camera.main;
        spriteRenderer = GetComponent<SpriteRenderer>();
        cb = FindObjectOfType<CameraBehaviour>();
        anim = GetComponent<Animation>();
        bossCollider= GetComponent<Collider2D>();
    }
    private void Start()
    {
        BossLifes = 85f;

    }
    private void Update()
    {
        //controllando che il nemico sia dentro il range della telecamera per iniziare a sparare
        Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
        if (cb.finalBoss && canFire && !gm.playerDed && !SecondPhaseChange)
        {
            //meccanica di fuoco nemico
            canBeKilled = true;
            canFire = false;
            Instantiate(Enemy1Projectile, FirePointEnemy1.position, FirePointEnemy1.rotation);
            Instantiate(Enemy2Projectile, FirePointEnemy1.position, FirePointEnemy1.rotation);
            Instantiate(Enemy3Projectile, FirePointEnemy1.position, FirePointEnemy1.rotation);

            StartCoroutine(DelayedAction(FireRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canFire = true;
            }
        }




    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disattivare i nemici uccisi (possono essere colpiti solo se all'interno della telecamera
        if (collision.tag == "Bullet" && canBeKilled)
        {
            BossLifes--;
            StartCoroutine(FlashRed());
            if(BossLifes == 40)
            {
                SecondPhaseChange= true;
                anim.Play();
                FireRate = 0.75f;
                bossCollider.enabled = false;
            }
            if (BossLifes <= 0)
            {
                gm.FinalBossAlive= false;                
                Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                Debug.Log("enemy ded");
            }
        }
        //controllo collisioni per disattivare i nemici non uccisi
        if (collision.tag == "EnemyBulletDestroy")
        {
            Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
            Debug.Log("enemy ded");

        }
    }

    private IEnumerator FlashRed()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
    }

    private void EndAnim()
    {
        anim.Stop();
        SecondPhaseChange = false;
        bossCollider.enabled = true;
    }
}
