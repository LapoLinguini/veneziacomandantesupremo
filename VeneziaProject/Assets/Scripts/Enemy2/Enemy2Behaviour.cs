using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Behaviour : MonoBehaviour
{
    [Header("Enemy Behaviour:")]
    [Tooltip("ampiezza verticale dell'OSCILLAZIONE del nemico")]
    public float high;
    [Tooltip("velocit� (frequenza) del movimento SINUSOIDALE del nemico")]
    public float frequency = 0f;
    [Tooltip("velocit� di fuoco del nemico")]
    public float FireRate = 1f;


    private Rigidbody2D rb;
    public GameObject DeathExplosionBig;
    public GameObject Enemy1Projectile;
    public Transform FirePointEnemy1;
   
    public bool canBeKilled = false;
    private bool canFire = true;
    private Camera cam;
    private GameManager gm;
    private SpriteRenderer spriteRenderer;

    public float WitherLifes = 25f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = Camera.main;
        gm = FindObjectOfType<GameManager>();
        WitherLifes = 20f;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        WitherLifes = 25f;
    }
    private void Update()
    {
        Vector2 rbvelocity = new Vector2();
        rbvelocity.y = high * Mathf.Sin(Time.time * frequency);
        rb.velocity = rbvelocity;

        //controllando che il nemico sia dentro il range della telecamera per iniziare a sparare
        Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
        if (viewPos.x < 1.05f && viewPos.x > 0 && canFire)
        {
            //meccanica di fuoco nemico
            canBeKilled = true;
            canFire = false;
            if(gm.playerDed == false)
            {
            Instantiate(Enemy1Projectile, FirePointEnemy1.position, FirePointEnemy1.rotation);
            }

            StartCoroutine(DelayedAction(FireRate));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                canFire = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disattivare i nemici uccisi (possono essere colpiti solo se all'interno della telecamera
        if (collision.tag == "Bullet" && canBeKilled)
        {
            WitherLifes--;
            StartCoroutine(FlashRed());
           
            if (WitherLifes <=0)
            {
                Instantiate(DeathExplosionBig, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                Debug.Log("enemy ded");
            }        
        }
        //controllo collisioni per disattivare i nemici non uccisi
        if (collision.tag == "EnemyBulletDestroy")
        {
            Instantiate(DeathExplosionBig, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
            Debug.Log("enemy ded");

        }
    }
    private IEnumerator FlashRed()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
    }
}
