using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Fire : MonoBehaviour
{

    private Rigidbody2D rb;
    public float bulletSpeed;
    public GameObject DeathExplosion;

    Vector2 ShotDirection = new Vector2();

    private void Awake()
    {
       
        rb = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        GameObject player = GameObject.Find("Player");
        ShotDirection = (player.transform.position - transform.position).normalized * bulletSpeed;
        

        rb.velocity = new Vector2(ShotDirection.x,ShotDirection.y);

    }
    private void Update()
    {
        
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disabilitare i proiettili
        if (collision.tag == "Player" || collision.tag == "Ground" || collision.tag == "EnemyBulletDestroy")
        {
            Instantiate(DeathExplosion, gameObject.transform.position, gameObject.transform.rotation);          
            gameObject.SetActive(false);
        }
    }
  
}
