using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public float playerLifes = 3f;
    
    public Collider2D playerTriggerCollider;

    public bool playerDed = false;

    public GameObject WinLoseMenuUI;
    public GameObject WinButton;
    public GameObject LoseButton;
    public bool FinalBossAlive = true;

    private Movement mv;
    private void Awake()
    {
        playerLifes = 3f;
        mv = FindObjectOfType<Movement>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Time.timeScale = 4f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Time.timeScale = 2f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Time.timeScale = 1f;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Time.timeScale = 0.5f;
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            playerTriggerCollider.enabled = false;
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            playerTriggerCollider.enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Screen.fullScreen= true;
        }
        if (Input.GetKeyDown(KeyCode.F) && Screen.fullScreen == true)
        {
            Screen.fullScreen = false;
        }

        if(playerDed)
        {
            WinLoseMenuUI.SetActive(true);
            LoseButton.SetActive(true);
        }
        if (!FinalBossAlive)
        {
            WinLoseMenuUI.SetActive(true);
            WinButton.SetActive(true);
            playerTriggerCollider.enabled = false;  
            mv.enabled = false;
        }

    }

}
