using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [Header("CameraSpeed")]
    [SerializeField] public float CameraSpeed = 0f;
    public bool finalBoss = false;
    private void Update()
    {
        //velocit� costante della camera
        gameObject.transform.Translate(new Vector2(CameraSpeed, 0) * Time.deltaTime, Space.World);

       if(transform.position.x >= 61)
        {
            CameraSpeed = 0f;
            transform.position = new Vector3(61, 0, -10);
            finalBoss= true;
            
        }
    }

}
