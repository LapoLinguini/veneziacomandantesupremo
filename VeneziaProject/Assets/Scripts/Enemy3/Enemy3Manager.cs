using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3Manager : MonoBehaviour
{
    private bool firstWaveStarted = false;

    public GameObject CreeperPref;

    private Vector2 enemySpawn = new Vector2();

    private float spawnX = 0f;

    private float enemySpawned = 0f;

    private bool canSpawn = true;

    private void Update()
    {
        Spawn();

        enemySpawn = new Vector2(transform.position.x + spawnX, Random.Range(-2.5f, 3f));

        if (enemySpawned >= 6)
        {
            canSpawn = false;
        }
    }

    private void Spawn()
    {
        if (firstWaveStarted && canSpawn)
        {
            firstWaveStarted = false;
            spawnX += 3.5f;
            enemySpawned++;
            Instantiate(CreeperPref, enemySpawn, gameObject.transform.rotation);

            StartCoroutine(DelayedAction(0));
            IEnumerator DelayedAction(float time)
            {
                yield return new WaitForSeconds(time);

                firstWaveStarted = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo triggerBox per startare una wave, checkare la distanza dal punto 0,0 mi pesava troppo lazyass
        if (collision.name == "Enemy3Spawn")
        {
            firstWaveStarted = true;
        }

    }
}
