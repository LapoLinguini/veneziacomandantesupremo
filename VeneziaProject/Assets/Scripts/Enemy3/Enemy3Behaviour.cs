using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3Behaviour : MonoBehaviour
{
    [Header("Enemy Behaviour:")]
    [Tooltip("ampiezza verticale dell'OSCILLAZIONE del nemico")]
    public float high;
    [Tooltip("velocit� (frequenza) del movimento SINUSOIDALE del nemico")]
    public float frequency = 0f;
    [Tooltip("velocit� di movimento ORIZZONTALE del nemico")]
    public float enemySpeed = 1f;

    private Rigidbody2D rb;
    public GameObject DeathExplosionBig;

    

    public bool canBeKilled = false;
    
    private Camera cam;
    private GameManager gm;
    private SpriteRenderer spriteRenderer;

    public float CreeperLifes = 6f;
    public Vector2 xvelocity = new Vector2();
    

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = Camera.main;
        gm = FindObjectOfType<GameManager>();   
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        CreeperLifes = 6f;
    }
    private void Update()
    {
        Vector2 rbvelocity = new Vector2();
        rbvelocity.y = high * Mathf.Sin(Time.time * frequency);
        rb.velocity = rbvelocity;

        transform.Translate(xvelocity * Time.deltaTime);
     

        //controllando che il nemico sia dentro il range della telecamera per iniziare a sparare
        Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
        if (viewPos.x < 1.025f && viewPos.x > 0)
        {
            
            canBeKilled = true;       
        }
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //controllo collisioni per disattivare i nemici uccisi (possono essere colpiti solo se all'interno della telecamera
        if (collision.tag == "Bullet" && canBeKilled)
        {
            CreeperLifes--;
            StartCoroutine(FlashRed());
            if (CreeperLifes <= 0)
            {
                Instantiate(DeathExplosionBig, gameObject.transform.position, gameObject.transform.rotation);
                gameObject.SetActive(false);
                Debug.Log("enemy ded");
            }
        }
        //controllo collisioni per disattivare i nemici non uccisi
        if (collision.tag == "EnemyBulletDestroy")
        {
            Instantiate(DeathExplosionBig, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
            Debug.Log("enemy ded");

        }
        if(collision.tag == "Player")
        {
            Instantiate(DeathExplosionBig, gameObject.transform.position, gameObject.transform.rotation);
            gameObject.SetActive(false);
        }
    }


    private IEnumerator FlashRed()
    {
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
    }

}
